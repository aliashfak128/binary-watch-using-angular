export class binaryHour{
    H32!:boolean ;
    H16!:boolean;
    H8!:boolean;
    H4!:boolean;
    H2!:boolean;
    H1!:boolean;
}
export class binaryMins{
    M32!:boolean;
    M16!:boolean;
    M8!:boolean;
    M4!:boolean;
    M2!:boolean;
    M1!:boolean;
}
export class binarySeconds{
    S32!:boolean;
    S16!:boolean;
    S8!:boolean;
    S4!:boolean;
    S2!:boolean;
    S1!:boolean;
}