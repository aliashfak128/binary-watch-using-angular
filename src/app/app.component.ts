import { Component } from '@angular/core';
import { binaryHour, binaryMins, binarySeconds } from './Model/watch';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'Binary-watch';
  // For Binary Hour
  HH =new binaryHour();
  MM =new binaryMins();
  SS = new binarySeconds();
  unCheckedHour(){
    // UnChecked All Hour
    this.HH.H32 =false;
    this.HH.H16 =false;
    this.HH.H8 =false;
    this.HH.H4 =false;
    this.HH.H2 =false;
    this.HH.H1 =false; 
  }
  unCheckedMins(){
    this.MM.M32 = false;
    this.MM.M16 = false;
    this.MM.M8 = false;
    this.MM.M4 =false;
    this.MM.M2 = false;
    this.MM.M1 = false;
  }
  unCheckedSeconds(){
    this.SS.S32 = false;
    this.SS.S16 = false;
    this.SS.S8 = false;
    this.SS.S4 = false;
    this.SS.S2 = false;
    this.SS.S1 = false;
  }
  setBinaryHour(Hour:number){
    this.unCheckedHour();
    if(Hour >= 32){
      this.HH.H32 =true;
      Hour = Hour - 32;
    }
    if(Hour >= 16){
      this.HH.H16 = true;
      Hour = Hour - 16;
    }
    if(Hour >= 8){
      this.HH.H8 = true;
      Hour = Hour - 8;
    }
    if(Hour >= 4){
      this.HH.H4 = true;
      Hour = Hour - 4;
    }
    if(Hour >= 2){
      this.HH.H2 = true;
      Hour = Hour - 2;
    }
    if(Hour >= 1){
      this.HH.H1 = true;
      Hour = Hour - 1;
    }
  }
  setBinaryMin(min:number){
      this.unCheckedMins();
      if(min >= 32){
        this.MM.M32 = true;
        min = min - 32;
      }
      if(min >= 16){
        this.MM.M16 = true;
        min = min - 16;
      }
      if(min >= 8){
        this.MM.M8 = true;
        min = min - 8;
      }
      if(min >= 4){
        this.MM.M4 = true;
        min = min - 4;
      }
      if(min >= 2){
        this.MM.M2 = true;
        min = min - 2;
      }
      if(min >= 1){
        this.MM.M1 = true;
        min = min - 1;
      }
  }
  setBinarySeconds(sec:number){
    this.unCheckedSeconds();
    if(sec >= 32){
      this.SS.S32 = true;
      sec = sec - 32;
    }
    if(sec >= 16){
      this.SS.S16 = true;
      sec = sec - 16;
    }
    if(sec >= 8){
      this.SS.S8 = true;
      sec = sec - 8;
    }
    if(sec >= 4){
      this.SS.S4 = true;
      sec = sec - 4;
    }
    if(sec >= 2){
      this.SS.S2 = true;
      sec = sec - 2;
    }
    if(sec >= 1){
      this.SS.S1 = true;
      sec = sec - 1
    }
  }
  showBinaryTime(){
    let Time = new Date();
    let currentHour:number = Time.getHours()
    let currentMin:number = Time.getMinutes();
    let currentSeconds:number = Time.getSeconds();
    this.setBinaryHour(currentHour);
    this.setBinaryMin(currentMin);
    this.setBinarySeconds(currentSeconds);
  }
  main(){  
    setInterval(()=>{
        this.showBinaryTime();
    },1000);
  }
  
  constructor(){
    this.main()
    
  }
}

